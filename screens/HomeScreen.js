import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  Button,
  View,
} from 'react-native';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <View style={styles.logo} >
        <Image
           source={
            require('../assets/images/Logosmall.png')
            }
            style={styles.img} 
           />
           </View>
          <View
            style={styles.scroll}
          >
          <View style={styles.logo} >
          <Image
           source={
            require('../assets/images/VV.png')
            }
            style={styles.img} 
           />
           </View>
          </View>
          <View style={styles.bt}>
          <Text style={styles.txet}>LAST CHANCE</Text>
          <Text style={styles.little}>1Day/2Hours/45Min</Text>
          <Button style={styles.btn} color="#CC9966" title="SHOP NOW" onPress={() => this.props.navigation.navigate("Products")} />
          </View>
          <View style={styles.bt}>
          <Text style={styles.txet}>PRIVATE SELL</Text>
          <Text style={styles.little}>All catégories</Text>
          <Button style={styles.btn} color="#CC9966" title="SHOP NOW" onPress={() => this.props.navigation.navigate("Products")} />
          </View>
          <View
            style={styles.scrolls}
          >
          </View>
          <View style={styles.btn} >
          <Text style={styles.txt}>ZARA</Text>
          <Text style={styles.tx}>Dress collection</Text>
          <Button style={styles.btn} color="#CC9966" title="SHOP NOW" onPress={() => this.props.navigation.navigate("Products")} />
          </View>

        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
  },
  title: {
    fontSize: 22,
    paddingBottom: 10,
  },
  little: {
    fontSize: 10,
    color: '#CC9966',
    alignItems: 'center',
    marginTop: -20,
    marginBottom: 20,
  },
  bt: {
    alignItems: 'center',
  },
  view: {
    width: '100%'
  },
  logo: {
    backgroundColor: '#000000',
    width: '100%',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: '#CC9966',
    marginTop:-2,
  },
  contentContainer: {
    alignItems: 'center', 
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcome: {
    width: 300,
  },
  scroll: {
    backgroundColor: '#f6f6f6',
  },
  scrolls: {
    marginTop: 20,
  },
  btn: {
    position: 'absolute',
    width: 300,
    height: 1000, 
    alignItems: 'center',
    marginTop: 250,
  },
  txt: {
   lineHeight:40,
   fontSize: 30,
   color: '#f8f8ff',
   marginBottom:20,
  },
  txet: {
    lineHeight:60,
    fontSize: 30,
    marginBottom:20,
    marginTop: 20,
    borderTopColor: '#CC9966',
    borderTopWidth: 1.5,
   },
  tx: {
   lineHeight:20,
   fontSize: 20,
   color: '#f8f8ff',
   marginBottom:20,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  helpLink: {
    paddingVertical: 15,
  },
  sliderImage: {
    height: 360,
    width: 360
  }
});
